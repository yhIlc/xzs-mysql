package com.mindskip.xzs.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.mindskip.xzs.domain.UserIntegral;


@Mapper
public interface UserIntegralMapper extends BaseMapper<UserIntegral> {

    /**
     * getUserIntegralByUserId
     *
     * @return List<User>
     */
    List<UserIntegral> getUserIntegralByUserId(Integer userId);
    
    /**
     * 判断是否已经存在积分
     * @param userIntegral
     * @return
     */
    UserIntegral exists(UserIntegral userIntegral);

   

    /**
     * userPageCount
     *
     * @param map map
     * @return Integer
     */
    Integer countUserIntegral(Integer userId);


    /**
     * insertUser
     *
     * @param user user
     */
    void insertUserIntegral(UserIntegral userIntegral);

}
