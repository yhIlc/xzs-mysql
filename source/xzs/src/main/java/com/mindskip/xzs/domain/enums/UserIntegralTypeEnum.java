package com.mindskip.xzs.domain.enums;


import java.util.HashMap;
import java.util.Map;

public enum UserIntegralTypeEnum {

	LOGIN("DL", "登陆"),
	XX("XX", "学习"),
	MR("MR", "每日答题"),
	GD("GD", "固定答题"),
	XS("XS", "限时答题"),
	PK("PK", "PK答题");
    String code;
    String name;

    UserIntegralTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    private static final Map<String, UserIntegralTypeEnum> keyMap = new HashMap<>();

    static {
        for (UserIntegralTypeEnum item : UserIntegralTypeEnum.values()) {
            keyMap.put(item.getCode(), item);
        }
    }

    public static UserIntegralTypeEnum fromCode(Integer code) {
        return keyMap.get(code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
