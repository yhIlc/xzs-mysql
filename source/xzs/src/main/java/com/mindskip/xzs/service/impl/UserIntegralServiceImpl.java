package com.mindskip.xzs.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindskip.xzs.domain.UserIntegral;
import com.mindskip.xzs.domain.enums.UserIntegralTypeEnum;
import com.mindskip.xzs.repository.UserIntegralMapper;
import com.mindskip.xzs.service.UserIntegralService;
import com.mindskip.xzs.utility.DateTimeUtil;

@Service
public class UserIntegralServiceImpl implements UserIntegralService {

	@Autowired
	private UserIntegralMapper userIntegralMapper;
	
	private static Logger log = LoggerFactory.getLogger(UserIntegralServiceImpl.class);
	@Override
	public void userIntegral(UserIntegralTypeEnum userIntegralTypeEnum, Integer userId, Integer integralNum) {
		
		UserIntegral record = new UserIntegral();
		Date date = new Date();
		record.setCreateDay(DateTimeUtil.dateShortFormat(date));
		record.setCreateTime(date);
		record.setIntegralNum(integralNum);
		record.setUserId(userId);
		record.setIntegralType(userIntegralTypeEnum.getCode());
		
		if(userIntegralTypeEnum == UserIntegralTypeEnum.LOGIN
				||
		  userIntegralTypeEnum == UserIntegralTypeEnum.MR
				) {
			UserIntegral exists = userIntegralMapper.exists(record);
			if(null != exists) {
				log.warn("账户:{},今天已经进行了:{},无法再次积分",userId,userIntegralTypeEnum.getName());	
				return;
			}
		}
		
		
		
		this.userIntegralMapper.insertUserIntegral(record);
	}

	@Override
	public Integer countUserIntegral(Integer userId) {
		return userIntegralMapper.countUserIntegral(userId);
	}

	@Override
	public List<UserIntegral> selectUserIntegralByUserId(Integer userId) {
		return userIntegralMapper.getUserIntegralByUserId(userId);
	}

	@Override
	public void userIntegral(UserIntegralTypeEnum userIntegralTypeEnum, Integer userId, Integer integralNum,
			String questionInfo) {
		UserIntegral record = new UserIntegral();
		Date date = new Date();
		record.setCreateDay(DateTimeUtil.dateShortFormat(date));
		record.setCreateTime(date);
		record.setIntegralNum(integralNum);
		record.setUserId(userId);
		record.setIntegralType(userIntegralTypeEnum.getCode());
		record.setQuestionInfo(questionInfo);
		
		if(userIntegralTypeEnum == UserIntegralTypeEnum.LOGIN
				||
		  userIntegralTypeEnum == UserIntegralTypeEnum.MR
				) {
			UserIntegral exists = userIntegralMapper.exists(record);
			if(null != exists) {
				log.warn("账户:{},今天已经进行了:{},无法再次积分",userId,userIntegralTypeEnum.getName());	
				return;
			}
		}
		
		
		this.userIntegralMapper.insertUserIntegral(record);
		
	}

	@Override
	public UserIntegral selectIntegral(Integer userId, UserIntegralTypeEnum type,String day) {
		UserIntegral userIntegral = new UserIntegral();
		userIntegral.setCreateDay(day);
		userIntegral.setUserId(userId);
		userIntegral.setIntegralType(type.getCode());
		return this.userIntegralMapper.exists(userIntegral);
	}

}
