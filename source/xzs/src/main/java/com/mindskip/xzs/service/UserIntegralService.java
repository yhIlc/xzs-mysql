package com.mindskip.xzs.service;

import java.util.List;

import com.mindskip.xzs.domain.UserIntegral;
import com.mindskip.xzs.domain.enums.UserIntegralTypeEnum;

public interface UserIntegralService {

	public void userIntegral(UserIntegralTypeEnum userIntegralTypeEnum, Integer userId, Integer integralNum);
	
	public void userIntegral(UserIntegralTypeEnum userIntegralTypeEnum, Integer userId, Integer integralNum,String questionInfo);

	
	public Integer countUserIntegral(Integer userId);
	
	public UserIntegral selectIntegral(Integer userId, UserIntegralTypeEnum type,String day);
	
	public List<UserIntegral> selectUserIntegralByUserId(Integer userId);
}
